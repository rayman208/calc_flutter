import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _firstNumber = '';
  String _secondNumber = '';
  double _result;

  String _text = '';
  String _sign;

  bool firstNumberIsSeted = false;

  void _addToCurrentText(String dopText) {
    setState(() {
      _text = _text + dopText;
    });
  }

  void _calculating() {
    double fn = double.parse(_firstNumber);
    double sn = double.parse(_secondNumber);

    setState(() {
      switch (_sign) {
        case '+':
          _result = fn + sn;
          _addToCurrentText('=$_result');
          break;
        case '-':
          _result = fn - sn;
          _addToCurrentText('=$_result');
          break;
        case '*':
          _result = fn * sn;
          _addToCurrentText('=$_result');
          break;
        case '/':
          if (sn != 0) {
            _result = fn / sn;
            _addToCurrentText('=$_result');
          } else {
            _text = 'Мать ебал...';
          }
          break;
      }
    });
  }

  void _clearText() {
    setState(() {
      firstNumberIsSeted = false;
      _text = '';
      _firstNumber = '';
      _secondNumber = '';
    });
  }

  void _setSign(String sign) {
    _sign = sign;
    firstNumberIsSeted = true;
    _addToCurrentText(sign);
  }

  void _numberClick(String number) {
    if (firstNumberIsSeted) {
      _secondNumber += number;
      _addToCurrentText(number);
    } else {
      _firstNumber += number;
      _addToCurrentText(number);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Text(
            '$_text',
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RaisedButton(onPressed: () => _setSign('+'), child: Text('+')),
              RaisedButton(onPressed: () => _setSign('-'), child: Text('-')),
              RaisedButton(onPressed: () => _setSign('*'), child: Text('*')),
              RaisedButton(onPressed: () => _setSign('/'), child: Text('/')),
            ],
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            RaisedButton(onPressed: () => _numberClick('1'), child: Text('1')),
            RaisedButton(onPressed: () => _numberClick('2'), child: Text('2')),
            RaisedButton(onPressed: () => _numberClick('3'), child: Text('3')),
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            RaisedButton(onPressed: () => _numberClick('4'), child: Text('4')),
            RaisedButton(onPressed: () => _numberClick('5'), child: Text('5')),
            RaisedButton(onPressed: () => _numberClick('6'), child: Text('6')),
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            RaisedButton(onPressed: () => _numberClick('7'), child: Text('7')),
            RaisedButton(onPressed: () => _numberClick('8'), child: Text('8')),
            RaisedButton(onPressed: () => _numberClick('9'), child: Text('9')),
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            RaisedButton(onPressed: _calculating, child: Text('=')),
            RaisedButton(onPressed: () => _numberClick('0'), child: Text('0')),
            RaisedButton(onPressed: _clearText, child: Text('C')),
          ]),
        ]),
      ),
    );
  }
}
